TF001: Tipicamente, a polarimetria baseia-se em ondas eletromagnéticas que foram refletidas por de algum material para caracterizar esse objeto.

TF002: Tipicamente, a polarimetria baseia-se em ondas eletromagnéticas que viajaram através de algum material para caracterizar esse objeto.

TF003: Na extermidade do tubo de um polarímetro há um prisma Nicol ou outro analizador.

TF004: Na extermidade do tubo de um polarímetro está umprisma Nicol ou outro polarizador.

TF005: (S,R) e (R,S) é um par de diastereoisômeros.

TF006: (S,R) e (R,S) é um par de enantiômeros.

TF007: O 5-bromoept-2-eno tem 4  estereoisômeros: (2 pares quirais de isômeros cis-trans).

TF008: O 5-bromoept-2-eno tem 3 estereoisômeros: (um par de enantiômeros e um composto meso).

TF009: Todo par de enantiômeros consiste em imagens especulares.

TF010: Em alguns casos, isômeros constitucionais são quirais.

TF011: Um composto meso é uma molécula sem um plano de simetria e com centros de quiralidade.

TF012: Um composto meso é uma molécula com um plano de simetria e centros de quiralidade.

TF013: Um composto meso é uma molécula sem um plano de simetria e nenhum centro de quiralidade.

TF014: Um composto meso é uma molécula com um plano de simetria e nenhum centro de quiralidade.

TF015: Um composto meso é uma substância que existe como uma mistura.

TF016: O descritor (2R,3S) descreve o composto meso do ácido tartárico,

TF017: O descritor (2R,3R) descreve o composto meso do ácido tartárico.

TF018: O descritor (2S,3S) descreve o composto meso do ácido tartárico.

TF019: Cada composto quiral tem um diastereisômero.

TF020: Para separar enantiômeros em uma solução via cromatografia, você precisa usar uma fase sólida que contenha quantidades de enantiômeros exatamente iguais.

TF021: Para separar enantiômeros em uma solução via cromatografia, você precisa usar uma fase sólida que contenha um simples enantiômero.

TF022: Se uma molécula tem um plano de simetria, ele não pode ter um enantiômero.

TF023: Se uma molécula não tem plano de simetria, ela é quiral.

TF024: (2R,3R)-3-cloropentan-2-ol e (2R,3S)-3-cloropentan-2-ol são um par de enantiômeros.

TF025: (2R,3R)-3-cloropentan-2-ol e (2R,3S)-3-cloropentan-2-ol são um par de diastereoisômeros.

TF026: Uma mistura racêmica não gira o plano da luz polarizada porque ela contém somente moléculas quirais com configuração R.

TF027: Uma mistura racêmica não gira o plano da luz polarizada porque para cada molécula que gira o plano em uma direção, há uma molécula (imagem especular) que roda o plano na mesma direção.

TF028: Uma mistura racêmica não gira o plano da luz polarizada porque ela contém somente moléculas quirais com configuração S.

TF029: Uma mistura racêmica não gira o plano da luz polarizada porque ela forma soluções opacas.

TF030: Uma mistura racêmica não gira o plano da luz polarizada porque para toda cada molécula que roda o plano em uma direção, há uma molécula (imagem especular) que gira o plano na direção oposta.

TF031: Quando um grupo OH é colocado em cada um dos 5 carbonos do pentano, 8 estereoisômeros (todos seriam oticamente ativos) serão possíveis para este composto.

TF032: Quando um grupo OH é colocado em cada um dos 5 carbonos do pentano, 4 estereoisômeros (3 seriam oticamente ativos) serão possíveis para este composto.

TF033: Quando um grupo OH é colocado em cada um dos 5 carbonos do pentano, 3 estereoisômeros (2 seriam oticamente ativos) serão possíveis para este composto.

TF034: Quando um grupo OH é colocado em cada um dos 5 carbonos do pentano, 7 estereoisômeros (6 seriam oticamente ativos) serão possíveis para este composto.

TF035: Quando um grupo OH é colocado em cada um dos 5 carbonos do pentano, 5 estereoisômeros (4 seriam oticamente ativos) serão possíveis para este composto.

TF036: Se o (R)-2-metilbutan-1-ol tem uma rotação específica de +5,75°, a rotação específica do (S)-2-metilbutan-1-ol é -5,75°.

TF037: Se o (R)-2-metilbutan-1-ol tem uma rotação específica de +5,75°, a rotação específica do (S)-2-metilbutan-1-ol é 0°.

TF038: Se o (R)-2-metilbutan-1-ol tem uma rotação específica de +5,75°, a rotação específica do (S)-2-metilbutan-1-ol é +4,75°.

TF039: Se o (R)-2-metilbutan-1-ol tem uma rotação específica de +5,75°, a rotação específica do (S)-2-metilbutan-1-ol deve ser determinada experimentalmente.

TF040: A rotação específica é independente da concentração.

TF041: A rotação específica é independente do caminho da luz do polarímetro.

TF042: A rotação observada é independente da concentração.

TF043: A rotação observada é independente do caminho da luz do polarímetro.

TF044: Acerca da relação entre configuração e rotação ótica, compostos R normalmente são dextrorrotatórios com poucas exceções.

TF045: Acerca da relação entre configuração e rotação ótica, compostos R sempre são dextrorrotatórios.

TF046: Acerca da relação entre configuração e rotação ótica, compostos R normalmente são levorrotatórios com poucas exceções.

TF047: Acerca da relação entre configuração e rotação ótica, compostos R sempre são levorrotatórios.

TF048: Acerca da relação entre configuração e rotação ótica, não há nenhuma relação.

TF049: O número máximo de estereoisômeros possíveis para um composto com cinco centros de quiralidade é igual a 5.

TF050: O número máximo de estereoisômeros possíveis para um composto com cinco centros de quiralidade é igual a 32.

TF051: Isômeros constitucionais têm a mesma fórmula molecular mas diferem na conectividade dos átomos.

TF052: Estereoisômeros têm a mesma conectividade de átomos mas diferem em seus arranjos espaciais.

TF053: Objetos quirais não são sobreponíveis em suas imagens especulares.

TF054: Objetos aquirais não são sobreponíveis em suas imagens especulares.

TF055: A fonte mais comum de quiralidade molecular é a presença de um centro de quiralidade, um carbono carregando 4 grupos diferentes.

TF056: Um polarímetro é um equipamento usado para medir a habilidade de compostos orgânicos quirais para girarem o plano da luz plano polarizado. Tais compostos são ditos serem oticamente inativos.

TF057: Um polarímetro é um equipamento usado para medir a habilidade de compostos orgânicos quirais para girarem o plano da luz plano polarizado. Tais compostos são ditos serem oticamente ativos.

TF058: A rotação específica de uma substância é uma propriedade física. Ela é determinada experimentalmente pela medida da rotação observada e dividindo pela concentração da solução e comprimento da cela.

TF059: Uma solução contendo um simples enantiômero é oticamente pura, enquanto uma solução contendo iguais quantidades de ambos enantiômeros é chamada de mistura racêmica.

TF060: Uma solução contendo um simples enantiômero é oticamente pura, enquanto uma solução contendo iguais quantidades de ambos enantiômeros é chamada de excesso enantiomérico.

TF061: Uma solução contendo um par de enantiômeros em desiguais quantidades é descrita em termos do excesso enantiomérico.

TF062: Uma solução contendo um par de enantiômeros em desiguais quantidades é descrita em termos da mistura racêmica.

TF063: Para um composto com múltiplos centros de quiralidade, uma família de estereoisômeros existe. Cada estereoisômero terá um enantiômero, com os membros remanescentes da família sendo diastereoisômeros.

TF064: Para um composto com múltiplos centros de quiralidade, uma família de estereoisômeros existe. Cada estereoisômero terá um diastereoisômero, com os membros remanescentes da família sendo enantiômeros.

TF065: Um composto que possui um plano de simetria será aquiral.

TF066: Um composto que possui um plano de simetria será quiral.

TF067: Um composto meso contém múltiplos centros de quiralidade mas é aquiral, porque ele possui simetria reflexional.

TF068: Resolução (separação) de enantiômeros pode ser ocorrer em um número de formas, incluindo o uso de agentes de resolução e cromatografia em coluna.

TF069: Compostos exibindo uma rotação positiva (+) são ditos serem dextrorrotatórios, enquanto compostos exibindo rotação negativa (-) são ditos levorrotatórios.

TF070: Um composto sem um plano de simetria será quiral (embora haja raras exceções).

TF0END