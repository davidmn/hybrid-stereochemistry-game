1) Le jeu hybride Stéréochimie a été conçu pour que 2-6 joueurs puissent jouer simultanément à chaque tour, ce qui rend le jeu plus dynamique.

2) Les joueurs doivent répondre à des questions au hasard à partir de quatre jeux de cartes afin de déplacer leurs pions de la position de départ à la position d'arrivée sur un parcours de 36 cases d’un plateau de jeu.

3) Jeu de cartes 1: Déterminer une affirmation comme étant vraie ou fausse.
Jeu de cartes 2: Déterminer un carbone asymétrique comme étant R ou S.
Jeu de cartes 3: Déterminer si deux composés sont : identiques, des isomères de constitution, des énantiomères ou des diastéréoisomères.
Jeu de cartes 4: Déterminer un composé comme étant chiral ou achiral.

4) Pour jouer, tous les joueurs du groupe doivent d'abord ouvrir l'application Stereochemistry Game, insérer le même code (n'importe quel nombre à 3 chiffres) et cliquer sur le bouton démarrer.

5) Par la suite, un joueur doit être choisi comme maître du jeu pour coordonner les actions et l’ordre des joueurs.

6) Après avoir reçu le signal du maître du jeu, tous les joueurs doivent cliquer sur le bouton question de l'application pour retourner une carte qui révèle une question à laquelle tous les joueurs peuvent répondre en même temps sur leur propre smartphone.

7) Lorsque tous les joueurs ont répondu aux questions, ils doivent vérifier la bonne réponse en cliquant sur le bouton réponse, et permettre à tous les adversaires de voir leur jeu.

8) Seuls ceux qui ont obtenu une bonne réponse peuvent déplacer leurs pions.

9) Après, chaque joueurs ayant obtenu la bonne réponse doit cliquer sur le bouton « dés » dans son application pour déterminer le nombre de cases dont il va déplacer son pion sur la piste.

10) Si le pion d’un joueur atterrit sur une case "chance ( ?)", le joueur doit cliquer sur la "carte chance" de l'application pour révéler une action qui doit être exécutée immédiatement après sa révélation.

11) Le jeu suit ce schéma et se termine lorsqu'un joueur atteint la position d'arrivée. Si deux joueurs ou plus atteignent la position d'arrivée en même temps, ils répondront successivement aux questions jusqu'à ce qu'un seul joueur soit le gagnant.
