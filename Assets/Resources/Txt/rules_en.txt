1) Hybrid Stereochemistry Game was designed for 2-6 players to play simultaneously in each turn making it more dynamic. 

2) The players must answer questions randomly from four decks to move their pieces from the start position up to the finish position of the board through 36 spaces in a track.

3) Deck 1: Classify a statement as True or False.
Deck 2: Define an asymmetric carbon as being R or S.
Deck 3: Define if two compounds are: same compounds, constitutional isomers, enantiomers or diastereoisomers.
Deck 4: Define a compound as chiral or achiral.

4) To play, initially, all players of the group must open the app Stereochemistry Game, insert the same code (any 3-digit number), and click on the Start button.

5) Afterward, a player must be defined as the game leader who will coordinate the actions and the order of the players.

6) After the command of the leader, all players must click on the card to flip it in the app by revealing a question that might be answered by all players at the same time in their own smartphones.

7) When all players have responded to the questions, they must check the correct answer by clicking on the answer button, and allow all opponents to see their play.

8) Only those who got the correct answer can move their pieces.

9) After, each player who got the correct answer must click on the “die” button in their app to draw the number of squares that they will move their piece on the track.

10) If a player’s piece lands on any “chance (?)” squares in the track, the player must click on the “chance card” in the app to reveal a command that must be executed immediately after its revelation.

11) The game follows this pattern and finishes when a player reaches the finish position of the board. If two or more players reach the finish position of the board at the same time, they will answer questions successively until only one player hits the answer and becomes the winner.