﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DebugCartas : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.GetComponent<TextMeshProUGUI>().text = ("Primeira carta: " + SeedScreen.cartas[0].ToString() + " / Resposta: " + SeedScreen.respostaCartas[0].ToString() + "\n " + "Segunda carta: " + SeedScreen.cartas[1].ToString() + " / Resposta: " + SeedScreen.respostaCartas[1].ToString());
    }
}
