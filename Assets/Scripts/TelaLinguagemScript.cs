﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TelaLinguagemScript : MonoBehaviour
{

    void Awake()
    {
        
        if (PlayerPrefs.HasKey("lang"))
        {
            SceneManager.LoadScene("TelaMenu");
        }

    }
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("lang"))
        {
            SceneManager.LoadScene("TelaMenu");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LinguaPt() {
        PlayerPrefs.SetInt("lang", 0);
        SceneManager.LoadScene("TelaMenu");
    }

    public void LinguaEn() {
        PlayerPrefs.SetInt("lang", 1);
        SceneManager.LoadScene("TelaMenu");
    }

    public void LinguaFr() {
        PlayerPrefs.SetInt("lang", 2);
        SceneManager.LoadScene("TelaMenu");
    }
}
