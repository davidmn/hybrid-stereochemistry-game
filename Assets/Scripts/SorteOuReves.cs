﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SorteOuReves : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
            gameObject.GetComponent<TextMeshProUGUI>().text = LangSorte(PlayerPrefs.GetInt("lang"));  
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public string LangSorte(int lang){
        Random.InitState(System.Environment.TickCount);
        int valor = Random.Range(0,7);
        string retorno;
        string[] frasesPt = new string[] {"Volte para o início.",
        "Avance duas casas.",
        "Avance três casas.",
        "Escolha um oponente para voltar duas casas.",
        "Retroceda duas casas.",
        "Escolha um oponente para avançar duas casas.",
        "Não jogar na próxima rodada."};

        string[] frasesEn = new string[] {"Go back to the start.",
        "Advance two steps.",
        "Advance three steps.",
        "Choose an oponent to return two steps.",
        "Retreat two steps.",
        "Choose an oponent to advance two steps.",
        "Do not play in the next turn."};

        string[] frasesFr = new string[] {"Retourner au départ.",
        "Avancer de 2 cases.",
        "Avancer de 3 cases.",
        "Faites reculer de 2 cases un joueur de votre choix.",
        "Reculer de 2 cases.",
        "Faites avancer de 2 cases un joueur de votre choix.",
        "Ne pas jouer au prochain tour."};

        switch (lang)
        {
            case 0:
            retorno = frasesPt[valor];
            break;
            case 1:
            retorno = frasesEn[valor];
            break;
            case 2:
            retorno = frasesFr[valor];
            break;            
            default:
            retorno = "error";
            break;
        }
        return retorno;
    }
}
