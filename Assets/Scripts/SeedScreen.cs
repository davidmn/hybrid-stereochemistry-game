﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SeedScreen : MonoBehaviour
{
    public GameObject inputField;

    public static int[] cartas = new int[280];  // 70 red, 70 green, 70 blue, 70 yellow
    public static int[] respostaCartas = new int[280];
    int[] respostaRed = { 1, 2, 1, 2, 1, 2, 2, 1, 2, 2, 1, 2, 1, 1, 1, 2, 1, 1, 1, 1, 2, 2, 1, 1, 2, 1, 1, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 2, 1, 2, 2, 2, 2, 1, 2, 1, 1, 2, 2, 1, 2, 1, 2, 1, 2, 1, 2, 2, 1, 2 };
    //RED: F = 1 ou V = 2
    int[] respostaGreen = { 1, 2, 2, 1, 1, 2, 1, 2, 2, 1, 1, 1, 2, 2, 2, 1, 1, 1, 2, 1, 2, 2, 1, 1, 2, 2, 2, 1, 1, 2, 1, 2, 2, 2, 2, 1, 1, 1, 1, 2, 1, 2, 1, 2, 2, 2, 2, 2, 2, 1, 2, 1, 2, 2, 1, 1, 1, 1, 2, 1, 2, 1, 1, 1, 2, 2, 2, 2, 1, 1 };
    //GREEN: R = 1 ou S = 2
    int[] respostaBlue = { 1, 0, 0, 0, 0, 1, 0, 1, 3, 2, 2, 0, 3, 1, 0, 3, 3, 3, 3, 0, 0, 1, 0, 1, 0, 1, 3, 1, 0, 1, 1, 0, 1, 0, 1, 3, 1, 0, 3, 2, 0, 0, 3, 1, 2, 0, 0, 2, 2, 1, 2, 1, 3, 3, 1, 3, 0, 1, 2, 1, 3, 3, 1, 0, 0, 1, 3, 1, 0, 2 };
    //BLUE: D = 0, E = 1, IC = 2, I = 3
    int[] respostaYellow = { 2, 2, 2, 2, 2, 2, 1, 1, 2, 2, 1, 2, 2, 1, 1, 2, 1, 1, 2, 1, 2, 2, 2, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 2, 2, 2, 1, 2, 1, 2, 2, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2, 1, 1, 2, 2, 2, 2, 1, 2, 1 };
    //YELLOW: Quiral = 1, Aquiral = 2
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GerarSeed(){
        string valorRandom;
        Random.InitState(System.Environment.TickCount);

        valorRandom = Random.Range(100,1000).ToString();

        inputField.GetComponent<TMP_InputField>().text = valorRandom; //insere um valor aleatorio no objeto de texto do campo de inserção da tela
        Debug.Log("Foi gerado o seed: " + valorRandom);
    }

    public void EmbaralharArray(int seed)
    {
        for (int i = 0; i < 280; i++)
        {
            //LAÇOS PARA INICIAR O ARRAY DE RESPOSTAS
            if (i < 70)
            {
                respostaCartas[i] = respostaRed[i];
            }
            else if (i > 69 && i < 140)
            {
                int v1 = i - 70;
                respostaCartas[i] = respostaGreen[v1];
            }
            else if (i > 139 && i < 210)
            {
                int v1 = i - 140;
                respostaCartas[i] = respostaBlue[v1];
            }
            else if (i > 209)
            {
                int v1 = i - 210;
                respostaCartas[i] = respostaYellow[v1];
            }

            //INICIALIZA O ARRAY DAS QUESTÕES
            int v2 = i + 1;
            cartas[i] = v2;
        }

        Random.InitState(seed);
        

        for (int i = 279; i > -1; i--)
        {
            int j = UnityEngine.Random.Range(0, cartas.Length);
            int temp = cartas[j];
            cartas[j] = cartas[i];
            cartas[i] = temp;

            int temp2 = respostaCartas[j];
            respostaCartas[j] = respostaCartas[i];
            respostaCartas[i] = temp2; 
        }

        PlayerPrefs.SetInt("cartaNumero", 1); //Inicializar a variável para exibir as cartas (vai ser incrementada a cada carta exibida)

        Debug.Log(cartas[0] + "-" + cartas[1] + "-" + cartas[2] + "-" + cartas[3] + "-" + cartas[4] + "-" + cartas[5]);
        Debug.Log(respostaCartas[0] + "-" + respostaCartas[1] + "-" + respostaCartas[2] + "-" + respostaCartas[3] + "-" + respostaCartas[4] + "-" + respostaCartas[5]);


    }

    public void VoltarTela()
    {
        SceneManager.LoadScene("TelaMenu");
    }

    public void AvancarTela(){
        if (inputField.GetComponent<TMP_InputField>().text.Length == 3 ) {
            PlayerPrefs.SetInt("seed", int.Parse(inputField.GetComponent<TMP_InputField>().text));

            Debug.Log("Foi usado o seed:" + PlayerPrefs.GetInt("seed").ToString());

            EmbaralharArray(PlayerPrefs.GetInt("seed"));

            SceneManager.LoadScene("TelaCarta");
        }
    }

}
