﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RegrasLanguage : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
         switch (PlayerPrefs.GetInt("lang"))
        {
            case 0:
            gameObject.GetComponent<TextMeshProUGUI>().text = Resources.Load<TextAsset>("Txt/rules_pt").ToString();  
            break;
            case 1:
            gameObject.GetComponent<TextMeshProUGUI>().text = Resources.Load<TextAsset>("Txt/rules_en").ToString(); 
            break;
            case 2:
            gameObject.GetComponent<TextMeshProUGUI>().text = Resources.Load<TextAsset>("Txt/rules_fr").ToString(); 
            break;
        

            default:
            break;
        }    
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
