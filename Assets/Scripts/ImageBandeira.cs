﻿using System.Net.Mime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageBandeira : MonoBehaviour
{
    public Texture2D imagemBandeiraAtiva;    
    public int activeLanguage;
    
    
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("lang")){
            if (PlayerPrefs.GetInt("lang") == activeLanguage){
                Sprite imagemBandeira = Sprite.Create(imagemBandeiraAtiva, new Rect(0.0f, 0.0f, imagemBandeiraAtiva.width, imagemBandeiraAtiva.height), new Vector2(0.5f, 0.5f), 100.0f);
                GetComponent<Image>().sprite = imagemBandeira;
            }
        }
       
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
