﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InfoPartida : MonoBehaviour
{
    public string ButtonPt1, ButtonPt2, ButtonEn1, ButtonEn2, ButtonFr1, ButtonFr2;
    // Start is called before the first frame update
    void Start()
    {
        switch (PlayerPrefs.GetInt("lang"))
        {
            case 0:
            gameObject.GetComponent<TextMeshProUGUI>().text = ButtonPt1 + PlayerPrefs.GetInt("seed").ToString() + ButtonPt2 + PlayerPrefs.GetInt("cartaNumero").ToString();  
            break;
            case 1:
            gameObject.GetComponent<TextMeshProUGUI>().text = ButtonEn1 + PlayerPrefs.GetInt("seed").ToString() + ButtonEn2 + PlayerPrefs.GetInt("cartaNumero").ToString();
            break;
            case 2:
            gameObject.GetComponent<TextMeshProUGUI>().text = ButtonFr1 + PlayerPrefs.GetInt("seed").ToString() + ButtonFr2 + PlayerPrefs.GetInt("cartaNumero").ToString();
            break;
    
            default:
            break;
        }  
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
