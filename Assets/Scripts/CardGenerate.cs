﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CardGenerate : MonoBehaviour
{
    public Texture2D imageDeckRed, imageDeckGreen, imageDeckBlue, imageDeckYellow, imageBotaoRespostaRed, imageBotaoRespostaGreen, imageBotaoRespostaBlue, imageBotaoRespostaYellow, imageBordaAcertou, imageBordaErrou;
    public GameObject card, contentImage, contentText, tituloRed, tituloGreen, tituloBlue, tituloYellow, buttonResposta, imageBordaResposta, buttonProxCarta, buttonDados, buttonSorteReves;
    public GameObject r1, r2, r3, r4, rSelecionada;
    public GameObject instrucao1, instrucao2, instrucao3, instrucao4Acertou, instrucao4Errou;
    public Texture2D imageRespostaGreen1Pt, imageRespostaGreen2Pt, imageRespostaYellow1Pt, imageRespostaYellow2Pt, imageRespostaRed2Pt, imageRespostaRed1Pt, imageRespostaBlue0Pt, imageRespostaBlue1Pt, imageRespostaBlue2Pt, imageRespostaBlue3Pt, imageRespostaBordaBotao;
    public Texture2D imageRespostaGreen1En, imageRespostaGreen2En, imageRespostaYellow1En, imageRespostaYellow2En, imageRespostaRed2En, imageRespostaRed1En, imageRespostaBlue0En, imageRespostaBlue1En, imageRespostaBlue2En, imageRespostaBlue3En;
    public Texture2D imageRespostaGreen1Fr, imageRespostaGreen2Fr, imageRespostaYellow1Fr, imageRespostaYellow2Fr, imageRespostaRed2Fr, imageRespostaRed1Fr, imageRespostaBlue0Fr, imageRespostaBlue1Fr, imageRespostaBlue2Fr, imageRespostaBlue3Fr;


    public static string deck;
    int cartaAtual, respostaSelecionada;
    Texture2D tex, tex2;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ExibeQuestao()
    {
        cartaAtual = SeedScreen.cartas[PlayerPrefs.GetInt("cartaNumero") - 1];

        Debug.Log("A " + PlayerPrefs.GetInt("cartaNumero") + "ª carta do deck tem valor: " + cartaAtual);

        if (PlayerPrefs.HasKey("cartaNumero"))
        {
            //DECK RED
            if (cartaAtual < 71)
            {
                deck = "red";
                tex = imageDeckRed;
                tex2 = imageBotaoRespostaRed;
                Debug.Log("Red de Numero: " + cartaAtual);
            }
            //DECK GREEN
            if (cartaAtual < 141 && cartaAtual > 70)
            {
                deck = "green";
                cartaAtual = cartaAtual - 70;
                tex = imageDeckGreen;
                tex2 = imageBotaoRespostaGreen;
                Debug.Log("Green de Numero: " + cartaAtual);
            }
            //DECK BLUE
            if (cartaAtual < 211 && cartaAtual > 140)
            {
                deck = "blue";
                cartaAtual = cartaAtual - 140;
                tex = imageDeckBlue;
                tex2 = imageBotaoRespostaBlue;
                Debug.Log("Blue de Numero: " + cartaAtual);
            }
            //DECK YELLOW
            if (cartaAtual > 210)
            {
                deck = "yellow";
                cartaAtual = cartaAtual - 210;
                tex = imageDeckYellow;
                tex2 = imageBotaoRespostaYellow;
                Debug.Log("Yellow de Numero: " + cartaAtual);
            }
            Sprite imagemCarta = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
            GetComponent<Image>().sprite = imagemCarta;

            Sprite imagemBotaoResposta = Sprite.Create(tex2, new Rect(0.0f, 0.0f, tex2.width, tex2.height), new Vector2(0.5f, 0.5f), 100.0f);
            buttonResposta.GetComponent<Image>().sprite = imagemBotaoResposta;

            SetImagem();
            SetTexto();
            AtivarBotoesResposta(deck, PlayerPrefs.GetInt("cartaNumero"));

            card.GetComponent<Button>().interactable = false;

            instrucao1.SetActive(false);
            instrucao2.SetActive(true);
            instrucao3.SetActive(false);
            instrucao4Acertou.SetActive(false);
            instrucao4Errou.SetActive(false);
        }
    }

    public void SetImagem()
    {
        if (deck != "red")
        {
            contentImage.SetActive(true);
            if (deck == "green")
            {
                tituloGreen.SetActive(true);
                Texture2D texTemp = Resources.Load<Texture2D>("Decks/DeckGreen/a-0" + cartaAtual.ToString("00"));
                Sprite spriteImagem = Sprite.Create(texTemp, new Rect(0.0f, 0.0f, texTemp.width, texTemp.height), new Vector2(0.5f, 0.5f), 100.0f);
                contentImage.GetComponent<Image>().sprite = spriteImagem;
            }

            if (deck == "blue")
            {
                tituloBlue.SetActive(true);
                Texture2D texTemp = Resources.Load<Texture2D>("Decks/DeckBlue/a-0" + cartaAtual.ToString("00"));
                Sprite spriteImagem = Sprite.Create(texTemp, new Rect(0.0f, 0.0f, texTemp.width, texTemp.height), new Vector2(0.5f, 0.5f), 100.0f);
                contentImage.GetComponent<Image>().sprite = spriteImagem;
            }

            if (deck == "yellow")
            {
                tituloYellow.SetActive(true);
                Texture2D texTemp = Resources.Load<Texture2D>("Decks/DeckYellow/a-0" + cartaAtual.ToString("00"));
                Sprite spriteImagem = Sprite.Create(texTemp, new Rect(0.0f, 0.0f, texTemp.width, texTemp.height), new Vector2(0.5f, 0.5f), 100.0f);
                contentImage.GetComponent<Image>().sprite = spriteImagem;
            }
            Debug.Log("O deck é o: " + deck + ". A carta é a: " + cartaAtual);
            Debug.Log(cartaAtual.ToString("00"));
        }
    }

    public void SetTexto()
    {
        TextAsset reader;

        if (deck == "red")
        {
            tituloRed.SetActive(true);
            contentText.SetActive(true);
            switch (PlayerPrefs.GetInt("lang"))
            {
                case (0):
                    reader = Resources.Load<TextAsset>("Decks/DeckRed/deck_red_PT");
                    break;
                case (1):
                    reader = Resources.Load<TextAsset>("Decks/DeckRed/deck_red_EN");
                    break;
                case (2):
                    reader = Resources.Load<TextAsset>("Decks/DeckRed/deck_red_FR");
                    break;
                default:
                    reader = Resources.Load<TextAsset>("Decks/DeckRed/deck_red_PT");
                    break;
            }

            string TextoDoDeck = reader.ToString();
            string toFind1 = "TF0" + cartaAtual.ToString("00") + ":";
            int start = TextoDoDeck.IndexOf(toFind1) + toFind1.Length;
            int end = TextoDoDeck.IndexOf("TF0", start);
            string StringPronta = TextoDoDeck.Substring(start, end - start);
            contentText.GetComponent<TextMeshProUGUI>().text = StringPronta;
        }
    }

    public void AtivarBotoesResposta(string corDeck, int numeroCarta)
    {
        switch (corDeck)
        {
            case "red":
                switch (PlayerPrefs.GetInt("lang"))
                {
                    case 0:
                        Sprite i1Pt = Sprite.Create(imageRespostaRed1Pt, new Rect(0.0f, 0.0f, imageRespostaRed1Pt.width, imageRespostaRed1Pt.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r2.GetComponent<Image>().sprite = i1Pt;
                        r2.SetActive(true);

                        Sprite i2Pt = Sprite.Create(imageRespostaRed2Pt, new Rect(0.0f, 0.0f, imageRespostaRed2Pt.width, imageRespostaRed2Pt.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r3.GetComponent<Image>().sprite = i2Pt;
                        r3.SetActive(true);
                        break;
                    case 1:
                        Sprite i1En = Sprite.Create(imageRespostaRed1En, new Rect(0.0f, 0.0f, imageRespostaRed1En.width, imageRespostaRed1En.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r2.GetComponent<Image>().sprite = i1En;
                        r2.SetActive(true);

                        Sprite i2En = Sprite.Create(imageRespostaRed2En, new Rect(0.0f, 0.0f, imageRespostaRed2En.width, imageRespostaRed2En.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r3.GetComponent<Image>().sprite = i2En;
                        r3.SetActive(true);
                        break;
                    case 2:
                        Sprite i1Fr = Sprite.Create(imageRespostaRed1Fr, new Rect(0.0f, 0.0f, imageRespostaRed1Fr.width, imageRespostaRed1Fr.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r2.GetComponent<Image>().sprite = i1Fr;
                        r2.SetActive(true);

                        Sprite i2Fr = Sprite.Create(imageRespostaRed2Fr, new Rect(0.0f, 0.0f, imageRespostaRed2Fr.width, imageRespostaRed2Fr.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r3.GetComponent<Image>().sprite = i2Fr;
                        r3.SetActive(true);
                        break;
                    default:
                        break;
                }

                break;

            case "green":
                switch (PlayerPrefs.GetInt("lang"))
                {
                    case 0:
                        Sprite i3Pt = Sprite.Create(imageRespostaGreen1Pt, new Rect(0.0f, 0.0f, imageRespostaGreen1Pt.width, imageRespostaGreen1Pt.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r2.GetComponent<Image>().sprite = i3Pt;
                        r2.SetActive(true);

                        Sprite i4Pt = Sprite.Create(imageRespostaGreen2Pt, new Rect(0.0f, 0.0f, imageRespostaGreen2Pt.width, imageRespostaGreen2Pt.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r3.GetComponent<Image>().sprite = i4Pt;
                        r3.SetActive(true);
                        break;
                    case 1:
                        Sprite i3En = Sprite.Create(imageRespostaGreen1En, new Rect(0.0f, 0.0f, imageRespostaGreen1En.width, imageRespostaGreen1En.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r2.GetComponent<Image>().sprite = i3En;
                        r2.SetActive(true);

                        Sprite i4En = Sprite.Create(imageRespostaGreen2En, new Rect(0.0f, 0.0f, imageRespostaGreen2En.width, imageRespostaGreen2En.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r3.GetComponent<Image>().sprite = i4En;
                        r3.SetActive(true);
                        break;
                    case 2:
                        Sprite i3Fr = Sprite.Create(imageRespostaGreen1Fr, new Rect(0.0f, 0.0f, imageRespostaGreen1Fr.width, imageRespostaGreen1Fr.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r2.GetComponent<Image>().sprite = i3Fr;
                        r2.SetActive(true);

                        Sprite i4Fr = Sprite.Create(imageRespostaGreen2Fr, new Rect(0.0f, 0.0f, imageRespostaGreen2Fr.width, imageRespostaGreen2Fr.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r3.GetComponent<Image>().sprite = i4Fr;
                        r3.SetActive(true);
                        break;

                    default:
                        break;
                }
                break;

            case "blue":
                switch (PlayerPrefs.GetInt("lang"))
                {
                    case 0:
                        Sprite i5Pt = Sprite.Create(imageRespostaBlue0Pt, new Rect(0.0f, 0.0f, imageRespostaBlue0Pt.width, imageRespostaBlue0Pt.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r1.GetComponent<Image>().sprite = i5Pt;
                        r1.SetActive(true);

                        Sprite i6Pt = Sprite.Create(imageRespostaBlue1Pt, new Rect(0.0f, 0.0f, imageRespostaBlue1Pt.width, imageRespostaBlue1Pt.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r2.GetComponent<Image>().sprite = i6Pt;
                        r2.SetActive(true);

                        Sprite i7Pt = Sprite.Create(imageRespostaBlue2Pt, new Rect(0.0f, 0.0f, imageRespostaBlue2Pt.width, imageRespostaBlue2Pt.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r3.GetComponent<Image>().sprite = i7Pt;
                        r3.SetActive(true);

                        Sprite i8Pt = Sprite.Create(imageRespostaBlue3Pt, new Rect(0.0f, 0.0f, imageRespostaBlue3Pt.width, imageRespostaBlue3Pt.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r4.GetComponent<Image>().sprite = i8Pt;
                        r4.SetActive(true);
                        break;

                    case 1:
                        Sprite i5En = Sprite.Create(imageRespostaBlue0En, new Rect(0.0f, 0.0f, imageRespostaBlue0En.width, imageRespostaBlue0En.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r1.GetComponent<Image>().sprite = i5En;
                        r1.SetActive(true);

                        Sprite i6En = Sprite.Create(imageRespostaBlue1En, new Rect(0.0f, 0.0f, imageRespostaBlue1En.width, imageRespostaBlue1En.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r2.GetComponent<Image>().sprite = i6En;
                        r2.SetActive(true);

                        Sprite i7En = Sprite.Create(imageRespostaBlue2En, new Rect(0.0f, 0.0f, imageRespostaBlue2En.width, imageRespostaBlue2En.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r3.GetComponent<Image>().sprite = i7En;
                        r3.SetActive(true);

                        Sprite i8En = Sprite.Create(imageRespostaBlue3En, new Rect(0.0f, 0.0f, imageRespostaBlue3En.width, imageRespostaBlue3En.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r4.GetComponent<Image>().sprite = i8En;
                        r4.SetActive(true);
                        break;

                    case 2:
                        Sprite i5Fr = Sprite.Create(imageRespostaBlue0Fr, new Rect(0.0f, 0.0f, imageRespostaBlue0Fr.width, imageRespostaBlue0Fr.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r1.GetComponent<Image>().sprite = i5Fr;
                        r1.SetActive(true);

                        Sprite i6Fr = Sprite.Create(imageRespostaBlue1Fr, new Rect(0.0f, 0.0f, imageRespostaBlue1Fr.width, imageRespostaBlue1Fr.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r2.GetComponent<Image>().sprite = i6Fr;
                        r2.SetActive(true);

                        Sprite i7Fr = Sprite.Create(imageRespostaBlue2Fr, new Rect(0.0f, 0.0f, imageRespostaBlue2Fr.width, imageRespostaBlue2Fr.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r3.GetComponent<Image>().sprite = i7Fr;
                        r3.SetActive(true);

                        Sprite i8Fr = Sprite.Create(imageRespostaBlue3Fr, new Rect(0.0f, 0.0f, imageRespostaBlue3Fr.width, imageRespostaBlue3Fr.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r4.GetComponent<Image>().sprite = i8Fr;
                        r4.SetActive(true);
                        break;

                    default:
                        break;
                }

                break;

            case "yellow":
                switch (PlayerPrefs.GetInt("lang"))
                {
                    case 0:
                        Sprite i9Pt = Sprite.Create(imageRespostaYellow1Pt, new Rect(0.0f, 0.0f, imageRespostaYellow1Pt.width, imageRespostaYellow1Pt.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r2.GetComponent<Image>().sprite = i9Pt;
                        r2.SetActive(true);

                        Sprite i10Pt = Sprite.Create(imageRespostaYellow2Pt, new Rect(0.0f, 0.0f, imageRespostaYellow2Pt.width, imageRespostaYellow2Pt.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r3.GetComponent<Image>().sprite = i10Pt;
                        r3.SetActive(true);
                        break;
                    case 1:
                        Sprite i9En = Sprite.Create(imageRespostaYellow1En, new Rect(0.0f, 0.0f, imageRespostaYellow1En.width, imageRespostaYellow1En.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r2.GetComponent<Image>().sprite = i9En;
                        r2.SetActive(true);

                        Sprite i10En = Sprite.Create(imageRespostaYellow2En, new Rect(0.0f, 0.0f, imageRespostaYellow2En.width, imageRespostaYellow2En.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r3.GetComponent<Image>().sprite = i10En;
                        r3.SetActive(true);
                        break;
                    case 2:
                        Sprite i9Fr = Sprite.Create(imageRespostaYellow1Fr, new Rect(0.0f, 0.0f, imageRespostaYellow1Fr.width, imageRespostaYellow1Fr.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r2.GetComponent<Image>().sprite = i9Fr;
                        r2.SetActive(true);

                        Sprite i10Fr = Sprite.Create(imageRespostaYellow2Fr, new Rect(0.0f, 0.0f, imageRespostaYellow2Fr.width, imageRespostaYellow2Fr.height), new Vector2(0.5f, 0.5f), 100.0f);
                        r3.GetComponent<Image>().sprite = i10Fr;
                        r3.SetActive(true);
                        break;
                    default:
                        break;
                }
                break;

            default:
                break;
        }

    }

    public void RespostaSelecionada(int valor)
    {
        switch (valor)
        {
            case 0:
                rSelecionada.transform.position = r1.transform.position;
                rSelecionada.SetActive(true);
                respostaSelecionada = 0;
                break;

            case 1:
                rSelecionada.transform.position = r2.transform.position;
                rSelecionada.SetActive(true);
                respostaSelecionada = 1;
                break;

            case 2:
                rSelecionada.transform.position = r3.transform.position;
                rSelecionada.SetActive(true);
                respostaSelecionada = 2;
                break;

            case 3:
                rSelecionada.transform.position = r4.transform.position;
                rSelecionada.SetActive(true);
                respostaSelecionada = 3;
                break;

            default:
                break;
        }

        buttonResposta.SetActive(true);

        instrucao1.SetActive(false);
        instrucao2.SetActive(false);
        instrucao3.SetActive(true);
        instrucao4Acertou.SetActive(false);
        instrucao4Errou.SetActive(false);
    }



    public void Voltar()
    {
        SceneManager.LoadScene("TelaMenu");
    }

    public void ExibeResposta()
    {
        Debug.Log("Resposta selecionada: " + respostaSelecionada + "Resposta da carta atual: " + SeedScreen.respostaCartas[(PlayerPrefs.GetInt("cartaNumero") - 1)] + "Carta atual: " + (PlayerPrefs.GetInt("cartaNumero") - 1));
        r1.SetActive(false);
        r1.GetComponent<Button>().interactable = false;
        r2.SetActive(false);
        r2.GetComponent<Button>().interactable = false;
        r3.SetActive(false);
        r3.GetComponent<Button>().interactable = false;
        r4.SetActive(false);
        r4.GetComponent<Button>().interactable = false;
        rSelecionada.SetActive(false);


        //Sprite spriteRSelecionada = Sprite.Create(imageRespostaBordaBotao, new Rect(0.0f, 0.0f, imageRespostaBordaBotao.width, imageRespostaBordaBotao.height), new Vector2(0.5f, 0.5f), 100.0f);
        //rSelecionada.GetComponent<Image>().sprite = spriteRSelecionada;

        //EXIBE BORDA COLORIDA E TITULO DIZENDO CERTO OU ERRADO
        if (respostaSelecionada == SeedScreen.respostaCartas[(PlayerPrefs.GetInt("cartaNumero") - 1)])
        {
            Sprite imagemborda = Sprite.Create(imageBordaAcertou, new Rect(0.0f, 0.0f, imageBordaAcertou.width, imageBordaAcertou.height), new Vector2(0.5f, 0.5f), 100.0f);
            imageBordaResposta.GetComponent<Image>().sprite = imagemborda;
            imageBordaResposta.SetActive(true);
            instrucao4Acertou.SetActive(true);
            Debug.Log("Acertou!");

            buttonDados.SetActive(true);
            buttonSorteReves.SetActive(true);
        }
        else
        {
            Sprite imagemborda = Sprite.Create(imageBordaErrou, new Rect(0.0f, 0.0f, imageBordaErrou.width, imageBordaErrou.height), new Vector2(0.5f, 0.5f), 100.0f);
            imageBordaResposta.GetComponent<Image>().sprite = imagemborda;
            imageBordaResposta.SetActive(true);
            instrucao4Errou.SetActive(true);
            Debug.Log("Errou!");
        }

        //EXIBE O ITEM DA RESPOSTA CERTA
        if (SeedScreen.respostaCartas[(PlayerPrefs.GetInt("cartaNumero") - 1)] == 0)
        {
            r1.SetActive(true);
            r1.transform.localPosition = new Vector3(347.9f, -535.57f, r1.transform.position.z);
            //rSelecionada.SetActive(true);
            rSelecionada.transform.position = new Vector3(r1.transform.position.x, r1.transform.position.y, r1.transform.position.z);
        }
        if (SeedScreen.respostaCartas[(PlayerPrefs.GetInt("cartaNumero") - 1)] == 1)
        {
            r2.SetActive(true);
            r2.transform.localPosition = new Vector3(347.9f, -535.57f, r2.transform.position.z);
            //rSelecionada.SetActive(true);
            rSelecionada.transform.position = new Vector3(r2.transform.position.x, r2.transform.position.y, r2.transform.position.z);
        }
        if (SeedScreen.respostaCartas[(PlayerPrefs.GetInt("cartaNumero") - 1)] == 2)
        {
            r3.SetActive(true);
            r3.transform.localPosition = new Vector3(347.9f, -535.57f, r3.transform.position.z);
            //rSelecionada.SetActive(true);
            rSelecionada.transform.position = new Vector3(r3.transform.position.x, r3.transform.position.y, r3.transform.position.z);
        }
        if (SeedScreen.respostaCartas[(PlayerPrefs.GetInt("cartaNumero") - 1)] == 3)
        {
            r4.SetActive(true);
            r4.transform.localPosition = new Vector3(347.9f, -535.57f, r4.transform.position.z);
            //rSelecionada.SetActive(true);
            rSelecionada.transform.position = new Vector3(r4.transform.position.x, r4.transform.position.y, r4.transform.position.z);
        }


        instrucao1.SetActive(false);
        instrucao2.SetActive(false);
        instrucao3.SetActive(false);

        buttonResposta.SetActive(false);
        buttonProxCarta.SetActive(true);

        //buttonDados.SetActive(true);
        //buttonSorteReves.SetActive(true);

    }

    public void ProxCarta()
    {
        int n = PlayerPrefs.GetInt("cartaNumero");
        n = n + 1;
        PlayerPrefs.SetInt("cartaNumero", n);

        SceneManager.LoadScene("TelaCarta");
    }
}
